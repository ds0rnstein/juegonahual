//UI ELEMENTS
	var winWidth, winHeight, nahual_container;
	var smallWheel, bigWheel, wcenTop, wcenBot;
	var calendar_container, datepicker, inputoverlay;
	var startbtn, bgl, bgr, blackoverlay, formoverlay;
	var zodiacon, zoname, zoimg, nahualname, nahualimg; 
	var nahualform, btnsubmit, contmail, fieldemail;
	var borderedcont, t1, t2, t3, arrowmail, fbc_container;
	var nahualfullform, fgender, fname, fbday, femail; 
	var formresult, picker, picker2, maincontainer, borderimage;

	var offsetY = 50;
	var startbuttonstate =  "";

	var nc_dim_h = 0;
	var nc_dim_w = 0;

	var supportsOrientationChange = "onorientationchange" in window; 
	var orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";

	var ANIMATIONS = {topWheelAnim:"", botWheelAnim:""};
	var animNames = {topWheelAnim:"topRotateTo", botWheelAnim:"botRotateTo"};

	var topItemsDeg = 360/13;
	var botItemsDeg = 360/20;

	var topIndex = 0;
	var botIndex = 0;

	var MAYAN_COUNT_EPOCH = 584282.5;
	var GREGORIAN_EPOCH = 1721425.5;

	var MAYAN_TZOLKIN_MONTHS = ["Imix", "Ik", "Akbal", "Kan", "Chicchan", "Kimi", "Manik", "Lamat", "Muluk", "Ok",
            "Chuen", "Eb", "Ben", "Ix", "Men", "Kid", "Kaban", "Etznab", "Kawak", "Ahau"];

	var MAYAN_HAAB_MONTHS = ["Pop", "Uo", "Zip", "Zotz", "Tzec", "Xul", "Yaxkin", "Mol", "Chen", "Yax", 
			"Zac", "Ceh", "Mac", "Kankin", "Muan", "Pax", "Kayab", "Cumku", "Uayeb"];
	
	var nah_dir = "https://ebookseuphoria.com/game/nawal";
	var nah_dir_email = "https://ebookseuphoria.staging.wpengine.com/readynmp";
	var nah_dir_registrar = "https://ebookseuphoria.staging.wpengine.com/inscripcion";

	var language = 0;

	var timeout_ongoing = false;
	var onMobile = false;
	var onLandingPage = false;
	var nahual_game_finished = false;
	var display_fullform = true;
	var firstTimeLoading = true;
	var game_started = false;

	var Nahual_Name_Value;
	var Nahual_Img_Value;

	var Mobilemode = "portrait";

	//DATE LANGUAGE ELEMENTS
	var en18n = {
            previousMonth : 'Previous Month',
            nextMonth     : 'Next Month',
            months        : ['January','February','March','April','May','June','July','August','September','October','November','December'],
			monthsShort   : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
            weekdays      : ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
            weekdaysShort : ['Sun','Mon','Tue','Wed','Thu','Fri','Sat']
        };

	var es18n = {
            previousMonth : 'Mes Anterior',
            nextMonth     : 'Mes Siguiente',
            months        : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
			monthsShort   : ['Ene','Feb','Mar','Abr','Mayo','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays      : ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort : ['Dom','Lun','Mar','Mier','Jue','Vie','Sab']
        };

	var br18n = {
            previousMonth : 'Mes Anterior',
            nextMonth     : 'Mes Siguiente',
            months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			monthsShort   : ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
            weekdays      : ['Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado'],
            weekdaysShort : ['Do','Se','Te','Qu','Qu','Se','Sa']
        };

    var fr18n = {
            previousMonth : 'Le mois précédent',
            nextMonth     : 'Le mois prochain',
            months        : ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
			monthsShort   : ['Jan','Fév','Mar','Avr','Mai','Jui','Juil','Août','Sep','Oct','Nov','Déc'],
            weekdays      : ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
            weekdaysShort : ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam']
        };

	//FORM LANGUAGE OPTS
	var frmes = {
			txt1: '¿Quieres conocer más acerca de tu nahual y lo que simboliza?',
			txt2: 'Los atributos que tiene y las compatibilidades posibles',
			txt3: 'Lo puedes recibir por correo',
			el1: 'Tu correo',
			el2: 'Mandar',
			mas: 'Masculino',
			fem: 'Femenino',
			nom: 'Coloque su Nombre',
			bday: 'Fecha de Nacimiento',
			res: 'Gracias por registrarte en VICENTA de a partir de este instante tu vida cambiará gracias a la energía MAYA , He enviado a tu correo electrónico la información de tu NAHUAL para la simbolización cósmica. Estaré enviándote a posterior un correo con mi primer escrito diseñado para tí.'
	};

	var frmen = {
			txt1: 'Do you want to know more about your nahual and what it symbolizes?',
			txt2: 'The attributes that it has and the possible compatibilities.',
			txt3: 'We will send it to your mail',
			el1: 'Your mail',
			el2: 'Send',
			mas: 'Male',
			fem: 'Female',
			nom: 'Enter your Name',
			bday: 'Birthday',
			res: 'Thank you for signing up with VICENTA. From now on, your life will change, thanks to the MAYAN energy. I have sent you an e-mail containing information about your NAHUAL for a cosmic symbolization. I will send you another e-mail with my first words for you.'
	};

	var frmfr = {
			txt1: 'Vous voulez en savoir beaucoup plus sur votre nahual et ce qu\'il symbolise?',
			txt2: 'Les attributs qu\' il possède et les possibles compatibilités.',
			txt3: 'Vous allez le recevoir par e-mail',
			el1: 'Votre e-mail',
			el2: 'Envoyer',
			mas: 'Masculin',
			fem: 'Féminin',
			nom: 'Mettez votre Prénom',
			bday: 'Date de Naissance',
			res: 'Merci pour votre inscription sur Dona Vicenta. D’ici peu, votre vie va changer grâce à l’énergie Maya. Je vous envoie de suite sur votre e-mail, tout ce qui concerne votre Nahual, votre énergie de naissance en fonction de la cosmologie Maya. Dans peu de temps, je vais vous envoyer un premier écrit sur votre e-mail, conçu uniquement pour vous.'
	};

	var frmbr = {
			txt1: 'Quer saber mais sobre o seu nahual e o que ele simboliza?',
			txt2: 'Os atributos que você tem e as possíveis compatibilidades.',
			txt3: 'Lhe enviaram por correio',
			el1: 'Teu correio',
			el2: 'Enviar',
			mas: 'Masculino',
			fem: 'Feminino',
			nom: 'Coloque seu Nome',
			bday: 'Data de Nascimiento',
			res: 'Agradecemos sua inscrição em VICENTA a partir deste momento sua vida vai mudar graças à energia MAYA, Foi enviado em  seu e-mail as informações do seu Nahual de simbolismo cósmico. Vou enviar-lhe depois de esse e-mail um outro com o meu primeiro escrito projetado para você.'
	};

	function getFormText(txt){
		return getLanFormText(language)[txt];
	}

	//ZODIAC LANGUAGE
	var zodNamesES = ["Acuario", "Piscis", "Aries", "Tauro", "Geminis", "Cancer", "Leo", "Virgo", "Libra", "Escorpio", "Sagitario", "Capricornio"];
	var zodNamesEN = ["Aquarius", "Pisces", "Aries", "Taurus", "Gemini", "Cancer", "Leo", "Virgo", "Libra", "Scorpio", "Sagittarius", "Capricorn"];
	var zodNamesBR = ["Aquário", "Peixes", "Áries", "Touro", "Gêmeos", "Câncer", "Leão", "Virgem", "Libra", " Escorpião", "Sagitário", "Capricórnio"];
	var zodNamesFR = ["Verseau", "Poissons", "Bélier", "Taureau", "Gémeaux", "Cancer", "Lion", "Vierge", "Balance", "Scorpion", "Sagittaire", "Capricorne"];
	var zodImgRES = ["acuario.png", "piscis.png", "aries.png", "tauro.png", "geminis.png", "cancer.png", "leo.png", "virgo.png", "libra.png", "escorpio.png", "sagitario.png", "capricornio.png"];
	var zoddir = "https://2380ie25r0n01w5tt7mvyi81-wpengine.netdna-ssl.com/wp-content/themes/flatsome/img/zodiaco/";

	function loadGame(){
		if(typeof(type_of_data) != "undefined"){
		switch(type_of_data){
			case "landing":
				display_fullform = true;
			break;
			case "text":
				display_fullform = false;
			break;
			case "text-nov":
				display_fullform = undefined;
			break;
			default:
				display_fullform = true;
			break;
		}
	}

		console.log("GAME READY!!!");
		setLanguage();
    	setGameContainer("init");
	}

	window.addEventListener(orientationEvent, function(){
		blackoverlay.style.display = "block";
		setTimeout(function(){
			formoverlay.style.display = "block";
			formoverlay.style.opacity = "0";
			setGameContainer(orientationEvent);
		}, 500);
	});
	
	function setLanguage(){
		language = getQueryVariable("li");

		if(!language){
			language = getQueryVariable("l_id");
		}

		if(!language){
			language = getQueryVariable("lang_id");
		}
		
		if(language == false){
			language = 1;
		}
	}

 	function setGameContainer(event){
		winWidth = window.innerWidth;
		winHeight = window.innerHeight;
		
		if(firstTimeLoading){
			initUIElements();
			preventOnDrag();
		}
		
		/*if(innerWidth > innerHeight){
				nahual_container.classList.add("nahual-desktop");
		}else{
				nahual_container.classList.remove("nahual-desktop");
		}*/

		if(/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
			onMobile = true;
			if(event === "orientationchange" || event === "resize"){
				blackoverlay.classList.remove("bgload");
				blackoverlay.classList.add("bgunload");
			}
		}

		if(winWidth < winHeight){
			Mobilemode = "portrait";
			maincontainer.style.width = winWidth + "px";
			maincontainer.style.height = winHeight + "px";
		}else{
			Mobilemode = "landscape";
			maincontainer.style.width = (winHeight*0.90*0.6042120551924473) + "px";
			maincontainer.style.height = (winHeight*0.90) + "px";
		}

		setPercDim(borderimage, maincontainer.offsetWidth, 1, maincontainer.offsetHeight, 1);
		
		nc_dim_h = maincontainer.offsetHeight * 0.95; // CONTAINER RATIO
		nc_dim_w = maincontainer.offsetWidth * 0.90; // CONTAINER RATIO

		setPercDim(nahual_container, nc_dim_w, 1, nc_dim_h, 1);

		noffsettop = maincontainer.offsetHeight * 0.025;
		noffsetleft = maincontainer.offsetWidth * 0.05;

		nahual_container.style.minHeight = nc_dim_h - noffsettop + "px";
		nahual_container.style.minWidth = nc_dim_w - noffsetleft + "px";

		nahual_container.style.top = noffsettop + "px";
		nahual_container.style.left = noffsetleft + "px";

		setPercDim(blackoverlay, nc_dim_w, 1, "width"); // BLACK SCREEN - LOADING OVERLAY

		setPercDim(formoverlay, nc_dim_w, 1, "width"); // CONTAINER - RESULT&FORM
		presetAnimation(formoverlay, "x", nc_dim_w, nc_dim_h);

		btnsubmit.value = getFormText("el2");

		setPercDim(smallWheel, nc_dim_h, 0.70, nc_dim_h, 0.70); // SMALL WHEEL SIZE
		centerHElement(smallWheel, nc_dim_w); 	// CENTER SMALL WHEEL

		setPercDim(bigWheel, nc_dim_h, 1.10, nc_dim_h, 1.10); //BIG WHEEL SIZE
		centerHElement(bigWheel, nc_dim_w); // CENTER BIG WHEEL

		setPercDim(wcenTop, nc_dim_h, 0.60, nc_dim_h, 0.60); //WHEEL CENTER TOP SIZE
		centerHElement(wcenTop, nc_dim_w); // CENTER WHEEL CENTER TOP

		setPercDim(zodiacon, parseFloat(wcenTop.style.width), 0.25, "width"); //ZODIAC CONTAINER SIZE
		setPercDim(zodiacon, parseFloat(wcenTop.style.width), 0.25, "height");

		centerHElement(zodiacon, nc_dim_w); // CENTER ZODIAC CONTAINER

		setPercDim(wcenBot, nc_dim_h, 0.45, nc_dim_h, 0.45);  //WHEEL CENTER BOTTOM SIZE
		centerHElement(wcenBot, nc_dim_w); // CENTER WHEEL CENTER BOTTOM

		var sbt_dim_w = bigWheel.offsetWidth * 0.40; //START BUTTON SIZE
		setPercDim(startbtn, bigWheel.offsetWidth, 0.30, sbt_dim_w, 0.3846); //START BUTTON SIZE
		centerHElement(startbtn, nc_dim_w); // CENTER START BUTTON

		setPercDim(datepicker, bigWheel.offsetWidth, 0.40, nc_dim_h, 0.10); //DATEPICKER SIZE

		setPercDim(calendar_container, parseFloat(datepicker.style.width), 1, parseFloat(datepicker.style.height), 3); // CALENDAR CONTAINER
		setPercDim(inputoverlay, parseFloat(datepicker.style.width), 1, parseFloat(datepicker.style.height), 1); // DATEPICKER INPUT OVERLAY

		positionHalfOffset("top", smallWheel, nc_dim_h, 0);
		positionHalfOffset("top", wcenTop, nc_dim_h, 0);
		positionHalfOffset("abstop", zodiacon, nc_dim_h, 0);
		positionHalfOffset("bottom", bigWheel, nc_dim_h, offsetY);
		positionHalfOffset("bottom", wcenBot, nc_dim_h, offsetY);
		positionHalfOffset("absbottom", startbtn, nc_dim_h, 0);

		belowElement(datepicker, smallWheel);
		centerHElement(datepicker, nc_dim_w);

		belowElement(inputoverlay, smallWheel);
		centerHElement(inputoverlay, nc_dim_w);

		belowElementWidth(calendar_container, datepicker);

		defineEvents();

		loadMainFormElements();
		
		datepicker.value = getFormText("bday");

		if(display_fullform != undefined){
			if(display_fullform){
				loadFullFormElements();
			}else{
				loadFormElements();
			}
		}else{
			formoverlay.style.display = "none";
		}

		btnsubmit.onclick = function(){
			if(validate()){
				if(display_fullform){
					requestRegisterForm(fgender.value, fname.value, fbday.value, femail.value);
				}else{
					requestEmailForm(fieldemail.value);
				}
			}
		};
		
		loadingComplete();
 	}

	 function setPercDim(el, ref, perc, axis){
		 if(axis === "width"){
			 el.style.width = ref*perc + "px";
		 }else if(axis === "height"){
			 el.style.height = ref*perc + "px";
		 }
	 }

	 function setPercDim(el, refx, percx, refy, percy){
			 el.style.width = refx*percx + "px";
			 el.style.height = refy*percy + "px";
	 }

	 function initUIElements(){
		maincontainer = document.getElementById("maincontnahual");

		nahual_container = document.getElementById("nahual-container");
		smallWheel = document.getElementById("swheel");
		bigWheel = document.getElementById("bwheel");
		wcenTop = document.getElementById("center-top");
		wcenBot = document.getElementById("center-bottom");
		startbtn = document.getElementById("start-btn");
		datepicker = document.getElementById("datpic");
		calendar_container = document.getElementById("calendar_container");
		inputoverlay = document.getElementById("inputoverlay");
		blackoverlay = document.getElementById("blackoverlay");
		zodiacon = document.getElementById("zodiac");
		zoimg = document.getElementById("zdimg");
		zoname = document.getElementById("ztext");
		formoverlay = document.getElementById("formoverlay");
		borderimage = document.getElementById("game-border");

		//Form Elements
		nahualname = document.getElementById("res-nahual-name");
		nahualimg = document.getElementById("res-nahual-img");
		nahualform = document.getElementById("nahual-form");
		btnsubmit = document.getElementById("btnsubmit");
		contmail = document.getElementById("contmail");
		fieldemail = document.getElementById("fieldemail");
		borderedcont = document.getElementById("borderedcont");
		arrowmail = document.getElementById("arrowmail");
		t1 = document.getElementById("t1");
		t2 = document.getElementById("t2");
		t3 = document.getElementById("t3");

		//Full Form Elements
		nahualfullform = document.getElementById("nahual-complete-form");
		fgender = document.getElementById("field-gender");
		fname = document.getElementById("field-name");
		fbday = document.getElementById("field-bday");
		femail = document.getElementById("field-email");
		fbc_container = document.getElementById("fbcalendar_container");
		formresult = document.getElementById("form-result-message");

		setPikadayForm1();
		setPikadayForm2();

		t1.innerHTML = getFormText('txt1');
		t2.innerHTML = getFormText('txt2');
		t3.innerHTML = getFormText('txt3');

		formresult.innerHTML = getFormText('res');

		document.getElementById("t12").innerHTML = getFormText('txt1');
		document.getElementById("t22").innerHTML = getFormText('txt2');

		arrowmail.innerHTML = getFormText('el1');

		document.getElementById('sel_opt_mas').innerHTML = getFormText('mas');
		document.getElementById('sel_opt_fem').innerHTML = getFormText('fem');
		
		bigWheel.classList.remove("rmovewheel");
		smallWheel.classList.remove("movewheel");
		inputoverlay.style.display = "block";
	}

	function loadMainFormElements(){
		borderedcont.style.width = formoverlay.offsetWidth-20 + "px";
		borderedcont.style.height = formoverlay.offsetHeight-20 + "px";
		centerElement(borderedcont, 20);

		btnsubmit.style.width = nc_dim_w * 0.45 + "px";
		btnsubmit.style.height = nc_dim_h * 0.08 + "px";
	}

	function loadFormElements(){
		nahualform.style.display = "table";
		var te_h = nc_dim_h * 0.05;

		t1.style.fontSize = (formoverlay.offsetHeight*0.03) + "px";
		t2.style.fontSize = (formoverlay.offsetHeight*0.03) + "px";
		t3.style.fontSize = (formoverlay.offsetHeight*0.03) + "px";

		formresult.style.fontSize = (formoverlay.offsetHeight*0.03) + "px";
		nahualname.style.fontSize = (formoverlay.offsetHeight*0.03) + "px";

		if(!nahual_game_finished){
			formoverlay.style.display = "none";
			formoverlay.style.opacity = "1";
			firstTimeLoading = false;
		}else{
			formoverlay.style.opacity = "1";
		}
	}

	 function loadFullFormElements(){
		fname.placeholder=getFormText("nom");
		fbday.placeholder=getFormText("bday");
		femail.placeholder=getFormText("el1");

		nahualfullform.style.display = "table";
		var te_h = nc_dim_h * 0.05;
		var el = document.getElementsByClassName("formfields");
		var fel = document.getElementsByClassName("fa");

		var elh = 0;
		
		nahualname.style.fontSize = (formoverlay.offsetHeight*0.03) + "px";

		document.getElementById("t12").style.fontSize = (formoverlay.offsetHeight*0.025) + "px";
		document.getElementById("t22").style.fontSize = (formoverlay.offsetHeight*0.025) + "px";
		formresult.style.fontSize = (formoverlay.offsetHeight*0.025) + "px";

		if(winWidth > winHeight){
			var nf_margin = (nahualfullform.offsetWidth * 0.05);

			for(i = 0; i < fel.length; i++){
				fel[i].style.fontSize = te_h + "px";
			}

			for(i = 0; i < el.length; i++){
				el[i].style.height = te_h + "px";
				elh = elh + el[i].offsetHeight;
			}
		}else{
			for(i = 0; i < fel.length; i++){
				fel[i].style.fontSize = te_h + "px";
			}

			for(i = 0; i < el.length; i++){
				el[i].style.height = te_h + "px";
				elh = elh + el[i].offsetHeight;
			}
		}

		fbc_container.style.top = fbday.offsetHeight + "px";
		fbc_container.style.width = fbday.offsetWidth + "px";
	
			if(!nahual_game_finished){
				formoverlay.style.display = "none";
				formoverlay.style.opacity = "1";
				firstTimeLoading = false;
			}else{
				formoverlay.style.opacity = "1";
			}
		}

	 function loadingComplete(){
		 blackoverlay.classList.remove("bgunload");
		 blackoverlay.classList.add("bgload");
		 window.scrollBy(0,1);
		 window.scrollBy(0,-1);
	 }	

	 function getQueryVariable(variable){
		 var query = window.location.search.substring(1);
		 var vars = query.split("&");
			for (var i=0;i<vars.length;i++) {
					var pair = vars[i].split("=");
					if(pair[0] == variable){return pair[1];}
			}
		return(false);
	  }

	 function preventOnDrag(){
		nahual_container.ondragstart = function(){ return false;};
		smallWheel.ondragstart = function(){ return false;};
		bigWheel.ondragstart = function(){ return false;};
		wcenTop.ondragstart = function(){ return false;};
		wcenBot.ondragstart = function(){ return false;};
		startbtn.ondragstart = function(){ return false;};
		datepicker.ondragstart = function(){ return false;};
		calendar_container.ondragstart = function(){ return false;};
		blackoverlay.ondragstart = function(){ return false;};
		zodiacon.ondragstart = function(){ return false;};
		zoimg.ondragstart = function(){ return false;};
		zoname.ondragstart = function(){ return false;};
	  }

	 function defineEvents(){
		 startbtn.addEventListener("click", function(){
			if(isDateEmpty()){
				this.src = "https://2380ie25r0n01w5tt7mvyi81-wpengine.netdna-ssl.com/wp-content/themes/flatsome/img/start_btn_active.png";
				cantRotate();
			}else{
				this.src = "https://2380ie25r0n01w5tt7mvyi81-wpengine.netdna-ssl.com/wp-content/themes/flatsome/img/start_btn_active.png";
				startGame();
			}
		 });
	 }

	 function isDateEmpty(){
		 if(datepicker.value === getFormText("bday")){
			return true;
		 }

		 if(datepicker.value.length == 0){
			return true;
		 }else{
			return false;
		 }
	 }

	 function addAnimPrefixCompat(elid, animationame, ndeg){
		animation = false,
		animationstring = 'animation',
		prefix = '',
		domPrefixes = 'Webkit Moz O ms'.split(' '),
		pfx  = '',
		keyframe = '',
		elm = document.querySelector(elid);

		if( elm.style.animationName ) { animation = true; }

		if( animation === false ) {
			for( var i = 0; i < domPrefixes.length; i++ ) {
				if( elm.style[ domPrefixes[i] + 'AnimationName'] !== undefined ) {
					pfx = domPrefixes[i];
					prefix = '-' + pfx.toLowerCase() + '-';
					animationstring = prefix + 'animation';
					animation = true;
					//initWheelAnimations(prefix, nahual);
					if(animationame === animNames.topWheelAnim){
						keyframe = '@' + prefix + 'keyframes '+ animNames.topWheelAnim +'{ '+
						'0% { ' + prefix + 'transform: rotate(0deg);}'+
						'100% { ' + prefix + 'transform: rotate(' + (1080+ndeg) + 'deg);}'+
						'}';
					}else if(animationame === animNames.botWheelAnim){
						keyframe = '@' + prefix + 'keyframes '+ animNames.botWheelAnim +'{ '+
						'0% { ' + prefix + 'transform: rotate(0deg);}'+
						'100% { ' + prefix + 'transform: rotate(' + (-1080-ndeg) + 'deg);}'+
						'}';
					}
					document.styleSheets[0].insertRule(keyframe, 0);
				}
			}
		}
	 }

	 function startGame(){
		 if(nahual_game_finished == true){return};
		 if(game_started == true){return};

		 game_started = true;

		 calculateNarhual();

		 addAnimPrefixCompat("#swheel", animNames.topWheelAnim, topItemsDeg * topIndex);
		 addAnimPrefixCompat("#bwheel", animNames.botWheelAnim, botItemsDeg * botIndex);
		 
		 var d = picker.getDate();
		 getZodiac(d.getDate(), d.getMonth()+1);

		 bigWheel.classList.add("rmovewheel");
		 smallWheel.classList.add("movewheel");
		 
		 if(display_fullform != undefined){
			setTimeout(function(){
				formoverlay.style.display = "block";
				formoverlay.classList.add("form_animEnter");
			}, 2500);
		 }else{
			 setTimeout(function(){
				executeScript();
			 }, 3000);
		 }

		 nahual_game_finished = true;
		 inputoverlay.style.display = "none";
		 var aurl = getTextUrl();
		 analytics.track("vicenta-nahual",{type:"game-start", url:aurl, version:"1.0",vidente_id:14,lang_id:language});
	 }

	 function cantRotate(){
		bigWheel.classList.add("cantRotate");
		smallWheel.classList.add("cantRotate");

		if(timeout_ongoing == false){
			timeout_ongoing = true;

			setTimeout(function(){
				if(bigWheel.classList.contains("cantRotate")){
					bigWheel.classList.remove("cantRotate");
				}

				if(smallWheel.classList.contains("cantRotate")){
					smallWheel.classList.remove("cantRotate");
				}

				startbtn.src = "https://2380ie25r0n01w5tt7mvyi81-wpengine.netdna-ssl.com/wp-content/themes/flatsome/img/start_btn.png";
				timeout_ongoing = false;
			}, 1000);
		}
	 }

	 function belowElement(c, d){
		 var top = parseFloat(d.style.top);
		 c.style.top = top + parseFloat(d.offsetHeight) + "px";
	 }

	 function belowElementWidth(c, d){
		 var top = parseFloat(d.style.top);
		 var left = parseFloat(d.style.left);
		 c.style.top = top + parseFloat(d.offsetHeight) + "px";
		 c.style.left = left + "px";
	 }

	 function centerElement(element, width, height){
		 var centerX = width/2;
		 var centerY = height/2;

		 var offsetX = parseFloat(element.style.width)/2;
		 var offsetY = parseFloat(element.style.height)/2;

		 element.style.left = centerX - offsetX + "px";
		 element.style.top = centerY - offsetY-7 + "px";
	 }
	 
	  function centerElement(element, offset){
		 element.style.left = (offset/2) - 3 + "px";
		 element.style.top = (offset/2) - 2 + "px";
	 }

	 function centerVElement(el, height){
		var centerY = height/2;
		var offsetY = parseFloat(el.offsetHeight)/2;
		
		el.style.top = centerY-offsetY + "px";
	 }

	 function centerVBetweenElements(ele, topEl, botEl, height){
		 var topElbot = parseFloat(topEl.style.height)/2;
		 var botEltop = height - parseFloat(botEl.style.height)/2;

		 ele.style.top = botEltop;
	}

	function centerHElement(element, width){
		 var centerX = width/2;
		 var offsetX = parseFloat(element.style.width)/2;

		 element.style.left = centerX-offsetX + "px";
	}

	function positionHalfOffset(type, element, height, offset){
		 var elcenY = parseFloat(element.style.height)/2;

		 if(type === "top"){
			 element.style.top = offset - elcenY + "px";
		 }else if(type === "abstop"){
		 	element.style.top == offset + 0 + "px";
		 }else if(type === "bottom"){
			 element.style.bottom = -elcenY + "px";
		 }else if(type === "absbottom"){
			 element.style.bottom = offset + 0 + "px";
		 }
	 }

	 function presetAnimation(elm, axis, contWidth, contHeight){
		x = 0,
		y = 0,
		keyframe = '';

		if(axis === "x"){
			x = contWidth;
			y = 0;
		}else if(axis === "y"){
			x = 0;
			y = contHeight
		}
			
		//PRE SET ANIMATION
		animation = false,
		animationstring = 'animation',
		prefix = '',
		domPrefixes = 'Webkit Moz O ms'.split(' '),
		pfx  = '';
		
		if( elm.style.animationName ) { animation = true; }

		if( animation === false ) {
			for( var i = 0; i < domPrefixes.length; i++ ) {
				if( elm.style[ domPrefixes[i] + 'AnimationName'] !== undefined ) {
					pfx = domPrefixes[i];
					prefix = '-' + pfx.toLowerCase() + '-';
					animationstring = prefix + 'animation';
					animation = true;

					if(axis === "x"){
						keyframe = '@' + prefix + 'keyframes formEnter{ '+
								'0% { ' + prefix + 'transform: translate(' + -x + "px, " +  0 + 'px);}'+
								'100% { ' + prefix + 'transform: translate(' + 0 + "px, " +  0 + 'px);}'+
								'}';
					}else if(axis === "y"){
						keyframe = '@' + prefix + 'keyframes formEnter{ '+
								'0% { ' + prefix + 'transform: translate(' + 0 + "px, " + -y + 'px);}'+
								'100% { ' + prefix + 'transform: translate(' + 0 + "px, " + 0 + 'px);}'+
								'}';
					}

					document.styleSheets[0].insertRule(keyframe, 1);
				}
			}
		}
	 }

	 // GET LANGUAGE FOR DATES
	 function getLanDate(id){
		switch(id){
			case '1':
				return es18n;
			break;
			case '2':
				return br18n;
			break;
			case '3':
				return fr18n;
			break;
			case '4':
				return en18n;
			break;
			default:
				return es18n;
		}
	}

	function getLanFormText(id){
		switch(id){
			case '1':
				return frmes;
			break;
			case '2':
				return frmbr;
			break;
			case '3':
				return frmfr;
			break;
			case '4':
				return frmen;
			break;
			default:
				return frmes;
		}
	}

	 function getCurrentDate(){
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1;
		var yyyy = parseInt(today.getFullYear());

		var date = {
			day:dd,
			month:mm,
			year:yyyy
		}

		return date;
	 }

	 function getZodiac(dia, mes){
		var n;

		if ((mes == 1 && dia > 19) || (mes == 2 && dia < 19)) {
			n = 0;
        }else if ((mes == 2 && dia > 18) || (mes == 3 && dia < 21)) {
			n = 1;
        }else if ((mes == 3 && dia > 20) || (mes == 4 && dia < 20)) {
            n = 2;
        }else if ((mes == 4 && dia > 19) || (mes == 5 && dia < 21)) {
            n = 3;
        }else if ((mes == 5 && dia > 20) || (mes == 6 && dia < 21)) {
            n = 4;
        }else if ((mes == 6 && dia > 20) || (mes == 7 && dia < 23)) {
            n = 5;
        }else if ((mes == 7 && dia > 22) || (mes == 8 && dia < 23)) {
			n = 6;
		}else if ((mes == 8 && dia > 22) || (mes == 9 && dia < 23)) {
			n = 7;
		}else if ((mes == 9 && dia > 22) || (mes == 10 && dia < 23)) {
			n = 8;
		}else if ((mes == 10 && dia > 22) || (mes == 11 && dia < 22)) {
			n = 9;
		}else if ((mes == 11 && dia > 21) || (mes == 12 && dia < 22)) {
			n = 10;
		}else if ((mes == 12 && dia > 21) || (mes == 1 && dia < 20)) {
			n = 11;
		}
		
		var i = zoddir + zodImgRES[n];
		
		zoimg.src = i;

		switch(language){
			case "1":
				zoname.innerHTML = zodNamesES[n];
			break;
			case "2":
				zoname.innerHTML = zodNamesEN[n];
			break;
			case "3":
				zoname.innerHTML = zodNamesBR[n];
			break;
			case "4":
				zoname.innerHTML = zodNamesFR[n];
			break;
			default:
				zoname.innerHTML = zodNamesEN[n];
		}

		setTimeout(function(){
			zodiacon.classList.add("zodload");
		}, 2000);
	 }

	 //=======================================
	 // 		NAHUAL CALCULATION
	 //=======================================
	
	 function gregorian_to_jd(year, month, day) {
		return (GREGORIAN_EPOCH - 1) +
			(365 * (year - 1)) +
			Math.floor((year - 1) / 4) +
			(-Math.floor((year - 1) / 100)) +
			Math.floor((year - 1) / 400) +
			Math.floor((((367 * month) - 362) / 12) +
			((month <= 2) ? 0 : (leap_gregorian(year) ? -1 : -2)) + day);
    }

	function leap_gregorian(year) {
		return ((year % 4) == 0) &&
			(!(((year % 100) == 0) && ((year % 400) != 0)));
	}

	function jd_to_mayan_count(jd) {
		var d, baktun, katun, tun, uinal, kin;
		d = jd - MAYAN_COUNT_EPOCH;
		baktun = Math.floor(d / 144000);
		d = mod(d, 144000);
		katun = Math.floor(d / 7200);
		d = mod(d, 7200);
		tun = Math.floor(d / 360);
		d = mod(d, 360);
		uinal = Math.floor(d / 20);
		kin = mod(d, 20);
		return new Array(baktun, katun, tun, uinal, kin);
    }

	function mod(a, b) {
		return a - (b * Math.floor(a / b));
    }

	function amod(a, b) {
		return mod(a - 1, b) + 1;
	}

	function jd_to_mayan_haab(jd) {
		var lcount, day;

		lcount = jd - MAYAN_COUNT_EPOCH;
		day = mod(lcount + 8 + ((18 - 1) * 20), 365);

		return new Array(Math.floor(day / 20) + 1, mod(day, 20));
    }

	function jd_to_mayan_tzolkin(jd) {
		var lcount = jd - MAYAN_COUNT_EPOCH;
		return new Array(amod(lcount + 20, 20), amod(lcount + 4, 13));
    }

	function setPikadayForm2(){
	  	if(typeof picker2 == "undefined"){
  			lan = getLanDate(language);
  			today = getCurrentDate();
  
  			minyear = today.year-100;
  			maxyear = today.year-18;
  			mxDate = new Date(maxyear, today.month-1, today.day);
  			minDate = new Date(minyear, 12, 1);
        
  			picker2 = new Pikaday({
  			field: document.getElementById('field-bday'),
  			firstDay: 1,
  			format: 'YYYY-MM-DD',
  			minDate: minDate,
  			maxDate: mxDate,
  			yearRange: [minyear, maxyear],
  			bound: true,
  			defaultDate: mxDate,
  			trigger: document.getElementById('fbinputoverlay'),
  			container: document.getElementById('fbcalendar_container'),
  			i18n: lan
  			});
	  	}
	}

	function setPikadayForm1(){
		if(typeof picker == "undefined"){
  			lan = getLanDate(language);
  			today = getCurrentDate();
  
  			minyear = today.year-100;
  			maxyear = today.year-18;
  			mxDate = new Date(maxyear, today.month-1, today.day);
  			minDate = new Date(minyear, 12, 1);
        
  			picker = new Pikaday({
  			field: document.getElementById('datpic'),
  			firstDay: 1,
  			format: 'MAIN',
  			minDate: minDate,
  			maxDate: mxDate,
  			yearRange: [minyear, maxyear],
  			bound: true,
  			defaultDate: mxDate,
  			trigger: document.getElementById('inputoverlay'),
  			container: document.getElementById('calendar_container'),
  			i18n: lan
  			});
		}
	}

	function calculateNarhual(){
            var j, year, mon, mday, hour, min, sec,
                weekday, hmindex,
                may_countcal, mayhaabcal, maytzolkincal;

            var date = picker.getDate();

            var pd = picker.getDate().getDate();
            var pm = picker.getDate().getMonth();
            var py = picker.getDate().getFullYear();

            year = new Number(py);

            weekday = jwday(j);
			
            mon = pm;
            mday = pd;
            hour = min = sec = 1;

            j = gregorian_to_jd(year, mon + 1, mday) +
            ((sec + 60 * (min + 60 * hour)) / 86400.0);

            //  Update Mayan Calendars
            may_countcal = jd_to_mayan_count(j);
            /*document.mayancount.baktun.value = may_countcal[0];
            document.mayancount.katun.value = may_countcal[1];
            document.mayancount.tun.value = may_countcal[2];
            document.mayancount.uinal.value = may_countcal[3];
            document.mayancount.kin.value = may_countcal[4];
            */

            mayhaabcal = jd_to_mayan_haab(j);
            maytzolkincal = jd_to_mayan_tzolkin(j);

			var nName = MAYAN_TZOLKIN_MONTHS[parseInt(maytzolkincal[0] - 1)];

			botIndex = getNarhualOnWheel(nName);
			topIndex = parseInt(maytzolkincal[1])-1;

			nahualname.innerHTML = nName;

			loadDoc(nName);
	}

	function jwday(j) {
            return mod(Math.floor((j + 1.5)), 7);
    }

	function getNarhualOnWheel(name){
			switch (name) {
					case "Cimi":
						return 0;
						break;
					case "Manik":
						return 1;
						break;
					case "Lamat":
						return 2;
						break;
					case "Muluc":
						return 3;
						break;
					case "Oc":
						return 4;
						break;
					case "Chuen":
						return 5;
						break;
					case "Eb":
						return 6;
						break;
					case "Ben":
						return 7;
						break;
					case "Ix":
						return 8;
						break;
					case "Men":
						return 9;
						break;
					case "Cib":
						return 10;
						break;
					case "Caban":
						return 11;
						break;
					case "Etznab":
						return 12;
						break;
					case "Cauac":
						return 13;
						break;
					case "Ahau":
						return 14;
						break;
					case "Imix":
						return 15;
						break;
					case "Ik":
						return 16;
						break;
					case "Akbal":
						return 17;
						break;
					case "Kan":
						return 18;
						break;
					case "Chicchan":
						return 19;
						break;
					default :
						return 0;
						break;
				}
		}

//AJAX
	function loadDoc(n) {
		nd = nah_dir;
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				var res = JSON.parse(this.response);
				Nahual_Img_Value = res.image;
				Nahual_Name_Value = res.name;

				nahualimg.src = Nahual_Img_Value;
				formoverlay.classList.add("form_animEnter");
			}
		};
		nd += "?l=" + language + "&n=" + n;

		xhttp.open("GET", nd, true);
		xhttp.send();
	}

	function executeScript(){
		var hidov = document.getElementsByClassName("lb_overlay");
		if(typeof hidov !== "undefined"){
			hidov[0].click();
		}else{
			console.log("no element found");	
		}

		window.api_controller.open();

		console.log("Sending Nahual! - new");
		
		da = datepicker.value.split(" ");
		mn = getMonthNumber(da[1]);
		var numbday = da[2] + "-" + mn + "-" + da[0]; 

		var o = {
    		name: Nahual_Name_Value,
    		img: Nahual_Img_Value,
			bday: numbday
		};

		window.api_controller.getNahual(JSON.stringify(o));
	}

	function getMonthNumber(monthname){
		return getLanDate(language).monthsShort.indexOf(monthname)+1;
	}


	 //=======================================
	 // 		RESULT FORM 
	 //=======================================

	 function requestEmailForm(n) {
		ne = nah_dir_email;
		var xhttp = new XMLHttpRequest();
		
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				var res = JSON.parse(this.response);

				nahualform.classList.add('bgload');
				btnsubmit.classList.add('bgload');

				setTimeout(function(){
					formresult.classList.add('zodload');
				}, 1800);
			}
		};

		ne += "?email=" + n;

		xhttp.open("GET", ne, true);
		xhttp.send();
	}
	
	function requestRegisterForm(gender, name, birth, email) {
		ne = nah_dir_registrar;
		var xhttp = new XMLHttpRequest();
		
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				var res = JSON.parse(this.response);

				nahualform.classList.add('bgload');
				btnsubmit.classList.add('bgload');

				setTimeout(function(){
					formresult.classList.add('zodload');
				}, 1800);
				
			}
		};

		ne += "?sexo=" + gender + "&prenom=" + name + "&bday=" + birth + "&email=" + email + "&ajax=ingreso-form";

		xhttp.open("GET", ne, true);
		xhttp.send();
		}

	 function validate(){
		 var elemiss = true;
		if(!display_fullform){
			if(!validateEmail(fieldemail.value)){
				fieldemail.classList.add("errorfield");
				elemiss = false;
			}else{
				fieldemail.classList.remove("errorfield");			
			}
	 	}else{			
			if(fbday.value === ""){
				fbday.classList.add("errorfield");
				elemiss = false;
			}else{
				fbday.classList.remove("errorfield");
			}

			if(!validateEmail(femail.value)){
				femail.classList.add("errorfield");
				elemiss = false;
			}else{
				femail.classList.remove("errorfield");
			}

			if(!validateName(fname.value)){
				fname.classList.add("errorfield");
				elemiss = false;
			}else{
				fname.classList.remove("errorfield");
			}
		 }

		 return elemiss;
	 }

	 function validateEmail(email) {
		return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email);
	}

	function validateName(vname){
		return /^[a-zàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœA-Z ]+$/.test(vname)
	}

	loadGame();