var calculadora = {
        calcGregorian: function () {
            this.updateFromGregorian();
        },
        gregorian_to_jd: function (year, month, day) {
            return (GREGORIAN_EPOCH - 1) +
                (365 * (year - 1)) +
                Math.floor((year - 1) / 4) +
                (-Math.floor((year - 1) / 100)) +
                Math.floor((year - 1) / 400) +
                Math.floor((((367 * month) - 362) / 12) +
                ((month <= 2) ? 0 :
                    (leap_gregorian(year) ? -1 : -2)
                ) +
                day);
        },
        MAYAN_COUNT_EPOCH: 584282.5,
        //  JD_TO_MAYAN_COUNT  --  Calculate Mayan long count from Julian day

        jd_to_mayan_count: function (jd) {
            var d, baktun, katun, tun, uinal, kin;

            d = jd - calculadora.MAYAN_COUNT_EPOCH;
            baktun = Math.floor(d / 144000);
            d = calculadora.mod(d, 144000);
            katun = Math.floor(d / 7200);
            d = calculadora.mod(d, 7200);
            tun = Math.floor(d / 360);
            d = calculadora.mod(d, 360);
            uinal = Math.floor(d / 20);
            kin = calculadora.mod(d, 20);

            return new Array(baktun, katun, tun, uinal, kin);
        },

        /*  MOD  --  Modulus function which works for non-integers.  */

        mod: function (a, b) {
            return a - (b * Math.floor(a / b));
        },

//  JD_TO_MAYAN_TZOLKIN  --  Determine Mayan Tzolkin "month" and day from Julian day

        MAYAN_TZOLKIN_MONTHS: new Array("Imix", "Ik", "Akbal", "Kan", "Chicchan",
            "Cimi", "Manik", "Lamat", "Muluc", "Oc",
            "Chuen", "Eb", "Ben", "Ix", "Men",
            "Cib", "Caban", "Etznab", "Cauac", "Ahau"),
//  JWDAY  --  Calculate day of week from Julian day

        Weekdays: new Array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'),

        NormLeap: new Array('Año normal', 'Año bisiesto'),
        jwday: function (j) {
            return calculadora.mod(Math.floor((j + 1.5)), 7);
        },
//  LEAP_GREGORIAN  --  Is a given year in the Gregorian calendar a leap year ?

        leap_gregorian: function (year) {
            return ((year % 4) == 0) &&
                (!(((year % 100) == 0) && ((year % 400) != 0)));
        },

        //  JD_TO_MAYAN_HAAB  --  Determine Mayan Haab "month" and day from Julian day

        MAYAN_HAAB_MONTHS: new Array("Pop", "Uo", "Zip", "Zotz", "Tzec", "Xul",
            "Yaxkin", "Mol", "Chen", "Yax", "Zac", "Ceh",
            "Mac", "Kankin", "Muan", "Pax", "Kayab", "Cumku", "Uayeb"),
        //  MAYAN_COUNT_TO_JD  --  Determine Julian day from Mayan long count

        

        jd_to_mayan_haab: function (jd) {
            var lcount, day;

            lcount = jd - calculadora.MAYAN_COUNT_EPOCH;
            day = calculadora.mod(lcount + 8 + ((18 - 1) * 20), 365);

            return new Array(Math.floor(day / 20) + 1, calculadora.mod(day, 20));
        },

        
        gregorian_to_jd: function (year, month, day) {
            return (calculadora.GREGORIAN_EPOCH - 1) +
                (365 * (year - 1)) +
                Math.floor((year - 1) / 4) +
                (-Math.floor((year - 1) / 100)) +
                Math.floor((year - 1) / 400) +
                Math.floor((((367 * month) - 362) / 12) +
                ((month <= 2) ? 0 :
                    (calculadora.leap_gregorian(year) ? -1 : -2)
                ) +
                day);
        }
        ,
//  AMOD  --  Modulus function which returns numerator if modulus is zero

        amod: function (a, b) {
            return calculadora.mod(a - 1, b) + 1;
        }
        ,

        jd_to_mayan_tzolkin: function (jd) {
            var lcount = jd - calculadora.MAYAN_COUNT_EPOCH;
            return new Array(calculadora.amod(lcount + 20, 20), calculadora.amod(lcount + 4, 13));
        }
        ,
        updateFromGregorian: function() {
            var j, year, mon, mday, hour, min, sec,
                weekday, hmindex,
                may_countcal, mayhaabcal, maytzolkincal;

            var date = picker.getDate();

            var pd = picker.getDate().getDate();
            var pm = picker.getDate().getMonth();
            var py = picker.getDate().getFullYear();

            window.alert(pd + "/" + pm + "/" + py);

            year = new Number(py);

            //  Update day of week in Gregorian box

            weekday = calculadora.jwday(j);
            //document.gregorian.wday.value = calculadora.Weekdays[weekday];
            mon = pm;
            mday = pd;
            hour = min = sec = 1;
            /*hour = new Number(document.gregorian.hour.value);
            min = new Number(document.gregorian.min.value);
            sec = new Number(document.gregorian.sec.value);*/
            //  Update leap year status in Gregorian box
            j = calculadora.gregorian_to_jd(year, mon + 1, mday) +
            ((sec + 60 * (min + 60 * hour)) / 86400.0);

            //document.gregorian.leap.value = calculadora.NormLeap[calculadora.leap_gregorian(year) ? 1 : 0];


            //  Update Mayan Calendars

            may_countcal = calculadora.jd_to_mayan_count(j);
            /*document.mayancount.baktun.value = may_countcal[0];
            document.mayancount.katun.value = may_countcal[1];
            document.mayancount.tun.value = may_countcal[2];
            document.mayancount.uinal.value = may_countcal[3];
            document.mayancount.kin.value = may_countcal[4];
            */
            window.alert("MAYAN: " + may_countcal[0] + "." + may_countcal[1] + "." + may_countcal[2] + "." + may_countcal[3] + "." + may_countcal[4]);

            mayhaabcal = calculadora.jd_to_mayan_haab(j);
            //document.mayancount.haab.value = "" + mayhaabcal[1] + " " + calculadora.MAYAN_HAAB_MONTHS[mayhaabcal[0] - 1];
            maytzolkincal = calculadora.jd_to_mayan_tzolkin(j);
            //document.mayancount.tzolkin.value = "" + maytzolkincal[1] + " " + calculadora.MAYAN_TZOLKIN_MONTHS[maytzolkincal[0] - 1];
            switch (maytzolkincal[1]) {
                case 1:
                    var text = 'Unidad';
                	var mum = 77;
                    break;
                case 2:
                    var text = 'Polaridad';
                	var mum = 70;
                    break;
                case 3:
                    var text = 'Ritmo';
                	var mum = 63;
                    break;
                case 4:
                    var text = 'Medida';
                	var mum = 55;
                    break;
                case 5:
                    var text = 'Centro';
                	var mum = 47;
                    break;
                case 6:
                    var text = 'Balance orgánico';
                	var mum = 40;
                    break;
                case 7:
                    var text = 'Poder místico';
                	var mum = 32;
                    break;
                case 8:
                    var text = 'Resonancia armónica';
                	var mum = 25;
                    break;
                case 9:
                    var text = 'Grandes ciclos';
                	var mum = 17;
                    break;
                case 10:
                    var text = 'Manifestación';
                	var mum = 9;
                    break;
                case 11:
                    var text = 'Disonancia';
                	var mum = 1;
                    break;
                case 12:
                    var text = 'Estabilidad compleja';
                	var mum = 94;
                    break;
                case 13:
                    var text = 'Movimiento universal';
                	var mum = 85;
                    break;


            }

			_text_last = calculadora.MAYAN_TZOLKIN_MONTHS[maytzolkincal[0] - 1];
          switch (calculadora.MAYAN_TZOLKIN_MONTHS[maytzolkincal[0] - 1]) {
                case "Imix":
                    var num = 3;
                    var text2 = 'Imix (Cocodrilo)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Imix.png';
                    break;
                case "Ik":
                    var num = 8;
                    var text2 = 'Ik (Viento)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Ik.png';
                    break;
                case "Akbal":
                    var num = 13;
                    var text2 = 'Akbal (Noche)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Akbal.png';
                    break;
                case "Kan":
                    var num = 18;
                    var text2 = 'Kan (Semilla)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Kan.png';
                    break;
                case "Chicchan":
                    var num = 23;
                    var text2 = 'Chicchan (Serpiente)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Chikchan.png';
                    break;
                case "Cimi":
                    var num = 28;
                    var text2 = 'Cimi (Muerte)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Kimi.png';
                    break;
                case "Manik":
                    var num = 33;
                    var text2 = 'Manik (Venado)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Manik.png';
                    break;
                case "Lamat":
                    var num = 38;
                    var text2 = 'Lamat (Conejo)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Lamat.png';
                    break;
                case "Muluc":
                    var num = 43;
                    var text2 = 'Muluc (Agua)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Muluk.png';
                    break;
                case "Oc":
                    var num = 48;
                    var text2 = 'Oc (Perro)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Ok.png';
                    break;
                case "Chuen":
                    var num = 53;
                    var text2 = 'Chuen (Mono)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Chuen.png';
                    break;
                case "Eb":
                    var num = 58;
                    var text2 = 'Eb (Camino)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Eb.png';
                    break;
                case "Ben":
                    var num = 62;
                    var text2 = 'Ben (Junco)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Ben.png';
                    break;
                case "Ix":
                    var num = 68;
                    var text2 = 'Ix (Jaguar)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Ix.png';
                    break;
                case "Men":
                    var num = 72;
                    var text2 = 'Men (Águila)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Men.png';
                    break;
                case "Cib":
                    var num = 78;
                    var text2 = 'Cib (Lechuza)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Kid.png';
                    break;
                case "Caban":
                    var num = 82;
                    var text2 = 'Caban (Tierra)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Kaban.png';
                    break;
                case "Etznab":
                    var num = 88;
                    var text2 = 'Etznab (Roca)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Etznab.png';
                    break;
                case "Cauac":
                    var num = 93;
                    var text2 = 'Cauac (Tormenta)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Kawak.png';
                    break;
                case "Ahau":
                    var num = 98;
                    var text2 = 'Ahau (Luz)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Ahau.png';
                    break;
                default :
                    var num = 3;
                    var text2 = '';
                    var img2 = '';
                    break;
            }


            if ($(".resultado_tzolkin").length > 0) {
                jQuery('.resultado_tzolkin').html('<div class="alert alert-success" style="text-align:center; position:relative; padding-bottom:30px; clear:both; display:block;"><h4 style="font-size:28px; margin-bottom:0; ">' + text2 + '</h4> Tono galáctico ' + maytzolkincal[1] + ' (' + text + ') <img style="float: right;height:90px;right: 20px;position: absolute;top: 20px;width: 85px;" src="' + img2 + '" title="' + text2 + '"></div>');
            }
            var array = {'num':num, 'text':text2, 'img':img2};
            return array;
        }

    }

    var signos = {

        horoscope: null,
        horoscopemaya: null,
      	signo_img:'',
        leta:0,
        lota:0,
        offset_marck:0,
        offset_h1:0,
        offset_h2:0,
        complete:0,
      	all_ones:0,
        dom: {},
     	 	i18n:{
              datepicker:{
                "es": {
                    previousMonth: "Previous Month",
                    nextMonth: "Next Month",
                    months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                    weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab']
                },
                "pt": {
                    previousMonth: "Previous Month",
                    nexWtMonth: "Next Month",
                    months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    weekdays: ['Domingo', 'Segunda-feira', 'Terça-Feira', 'Quarta-Feira', 'Quinta-Feira', 'Sexta-Feira', 'Sábado'],
                    weekdaysShort: ['Dom', 'Segf', 'Tef', 'Quf', 'Qui', 'Sexf', 'Sa']
                },
                "fr": {
                    previousMonth: "Previous Month",
                    nextMonth: "Next Month",
                    months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                    weekdays: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                    weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam']
                },
                "en": {
                    previousMonth: "Previous Month",
                    nextMonth: "Next Month",
                    months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    weekdays: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                    weekdaysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
                }
            }
        },
        change_month:function(){
            var cont = '';
          	var l = {1:"es",2:"pt",3:"fr",4:"en"};
          	var array = signos.i18n.datepicker[l[1]];
          	for(var x=1;x<=12;x++)
            {
              	cont+='<option value="'+x+'">'+array.months[x-1]+'</option>';              	
            }
          	$('#month').append(cont);
        },
        cache: function (selector) {
            if (undefined === this.dom[selector])
                this.dom[selector] = $(selector);

            return this.dom[selector];
        },

        signo_zodiaco: function () {
            var zodiaco = '';


            var dia = signos.cache("#day").val();
            var mes = signos.cache("#month").val();
            var year = signos.cache("#year").val();

            if ((mes == 1 && dia > 19) || (mes == 2 && dia < 19)) {
                zodiaco = 0;
              	signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/acuario.png';
            }
            else if ((mes == 2 && dia > 18) || (mes == 3 && dia < 21)) {
                zodiaco = 78;
              signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/pisis.png';
            }
            else if ((mes == 3 && dia > 20) || (mes == 4 && dia < 20)) {
                zodiaco = 70;
              signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/aries.png';
            }
            else if ((mes == 4 && dia > 19) || (mes == 5 && dia < 21)) {
                zodiaco = 63;
              signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/tauro.png';
            }
            else if ((mes == 5 && dia > 20) || (mes == 6 && dia < 21)) {
                zodiaco = 55;
              signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/geminis.png';
            }
            else if ((mes == 6 && dia > 20) || (mes == 7 && dia < 23)) {
                zodiaco = 47;
              signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/junio.png';
            }
            else if ((mes == 7 && dia > 22) || (mes == 8 && dia < 23)) {
                zodiaco = 40;
              signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/leo.png';
            }
            else if ((mes == 8 && dia > 22) || (mes == 9 && dia < 23)) {
                zodiaco = 32;
              signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/virgo.png';
            }
            else if ((mes == 9 && dia > 22) || (mes == 10 && dia < 23)) {
                zodiaco = 25;
              signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/libra.png';
            }
            else if ((mes == 10 && dia > 22) || (mes == 11 && dia < 22)) {
                zodiaco = 18;
              signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/escorpion.png';
            }
            else if ((mes == 11 && dia > 21) || (mes == 12 && dia < 22)) {
                zodiaco = 10;
              signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/sagitario.png';
            }
            else if ((mes == 12 && dia > 21) || (mes == 1 && dia < 20)) {
                zodiaco = 1;
              signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/capricornio.png';
            }

            return zodiaco;

        },

        set_html: function (value, index, angle, unit) {

            var html = ''
                , val = value;

            if (unit !== '') {
                val += unit;
            }

            html += '<b>Value: </b>' + val + '<br/>';
            html += '<b>Index: </b>' + index + '<br/>';
            html += '<b>Angle: </b>' + angle + '<br/>';

            return html;
        },

        maya:{'text':''},
        startSlider:function(){
            signos.maya = calculadora.updateFromGregorian();
            signos.horoscopomaya = $('#horoscopomaya').roundSlider({
                min: 0,
                max: 100,
                radius: 200,
                width: 0,
                value: 0,
                handleSize: 10,
    			editableTooltip: true,
                tooltipFormat:function(e){
                    return signos.maya.text;
                }

            });

             signos.horoscope = $('#horoscope').roundSlider({
                        min: 0,
                        max: 100,
                        radius: 90,
                        width: 0,
                        value: 0,
               editableTooltip: true,
               drag:function(e){
                 console.log(e);
               },
                        editableTooltip: true,
                        handleSize:10,
                        tooltipFormat: "handleHoroscope"
                    });
        },
      	all_back:function(f){
            var en1 = $('#engranage1'),en2 = $('#engranage2'),en3 = $('#engranage3'),en4 = $('#engranage4'),en5 = $('#engranage5'), en6 = $('#engranage6'), a = 0,b = 0 , c = 0, d= 0, f=0,h=0;
          	var global = 0.3;
          var func_1 = function(){
          	en1.animate({'rotate':a+=global},300,function(){	
                  func_1();
                });
          }
          
          var func_2 = function(){
          	en2.animate({'rotate':b-=global},300,function(){	
                  func_2();
                });
          }
          
          var func_3 = function(){
          	en3.animate({'rotate':c+=global},300,function(){	
                  func_3();
                });
          }
          
          var func_4 = function(){
          	en4.animate({'rotate':d+=global},300,function(){	
                  func_4();
                });
          }
          
          var func_5 = function(){
          	en5.animate({'rotate':f-=global},300,function(){	
                  func_5();
                });
          }
          
          var func_6 = function(){
          	en6.animate({'rotate':h-=global},300,function(){	
                  func_6();
                });
          }
          console.log(f);
 			if(!f)
            {
              func_1();
              func_2();
              func_3();
              func_4();
              func_5();
              func_6();
            }else{
              alert("hola");
          		$('#engranage1').stop(); 
            }
          
          
          
          
    	},
        events: function () {
            var afen,aufen;
            signos.cache("#calcular").click(function () {
              $(this).prop("disabled",true);
              signos.all_back();
                var zodiaco = signos.signo_zodiaco();
                signos.horoscope.roundSlider({value: zodiaco});
                signos.maya = calculadora.updateFromGregorian();
                signos.horoscopomaya.roundSlider({value: signos.maya.num});
                signos.leta = signos.horoscopomaya.css('rotate');
                signos.lota = signos.horoscope.css('rotate');
                signos.offset_marck = parseInt($('#marck').offset().left);
              	$('#signos-zodiacales').prop('src',signos.signo_img);
             	 var func = function(){
              	signos.leta = signos.leta+0.04;                  
                signos.offset_h1 = parseInt($('.rs-handle').eq(0).position().left);
                if((signos.offset_h1>=188 && signos.offset_h1<=198) && $('.rs-handle').eq(0).position().top>=-1.5 && $('.rs-handle').eq(0).position().top<=-1.13){
                  	$('#engranage1').stop(true);
                  	$('#engranage2').stop(true);
                  	$('#engranage3').stop(true);
                  	$('#engranage4').stop(true);
                  	$('#engranage5').stop(true);
                  	$('#engranage6').stop(true);
                	signos.horoscopomaya.stop();
                  setTimeout(function(){
                    	_datepicker();
                    	_validator_form("formulario-popup");
                    	$("#calcular").prop("disabled",true);
                  		$('.ruedas,#exittender-title').fadeOut();
                    	$('#nawal').addClass("nawal-choce");
                    	$('.fillform,.nawales').fadeIn();
                    	$("#exittender-title2").fadeIn();
                  },1000);
                  	
                  	return true;
                }
                
                signos.horoscopomaya.animate({'rotate':signos.leta},50,function(){	
                  func();
                });
               
              }
              
              var func_2 = function(){
              	signos.lota = signos.lota+0.04;                  
                signos.offset_h2 = parseInt($('.rs-handle').eq(1).position().left);
                if((signos.offset_h2 >= 86 && signos.offset_h2 <= 89) && $('.rs-handle').eq(1).position().top>11){
                  	signos.horoscope.stop();
                  	func();
                  	return true;
              	}
                signos.horoscope.animate({'rotate':signos.lota},50,function(){	
                  func_2();
                });
               
              }
              
              
              setTimeout(func_2,1500);

                signos.cache("#popover").fadeIn(200);
                signos.cache("#nombre").text(_text_last);
                signos.cache("#imagenmaya").attr( 'src', signos.maya.img);
                signos.cache("#descripcion").text( 'Descripcion del signo maya '+signos.maya.text+' el cual sera dinamico segun el idioma ');

            });

        },
        init: function () {
			this.change_month();
            this.startSlider();
            this.events();
        }


    };

    function handleHoroscope(args) {
        var signo;
        switch (args.value) {
            case 0:
                signo = "Acuario";
            	signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/acuario.png';
                break;
            case 1:
                signo = "Piscis";
            	signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/pisis.png';
                break;
            case 2:
                signo = "Aries";
            	signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/aries.png';
                break;
            case 3:
                signo = "Tauro";
            	signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/tauro.png';
                break;
            case 4:
                signo = "Géminis";
            	signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/geminis.png';
                break;
            case 5:
                signo = "Cáncer";
            signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/cancer.png';
                break;
            case 6:
                signo = "Leo";
            signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/leo.png';
                break;
            case 7:
                signo = "Virgo";
            signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/virgo.png';
                break;
            case 8:
                signo = "Libra";
            signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/libra.png';
                break;
            case 9:
                signo = "Escorpio";
                break;
            case 10:
                signo = "Sagitario";
            signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/sagitario.png';
                break;
            case 11:
                signo = "Capricornio";
            signos.signo_img = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/signos-zodiacales/capricornio.png';
                break;

            default:
                signo = "";

        }
        return signo;
    }

    	function getNarhual(name){
		switch (name) {
                case "Imix":
                    /*var num = 3;
                    var text2 = 'Imix (Cocodrilo)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Imix.png';*/
                    break;
                case "Ik":
                    /*var num = 8;
                    var text2 = 'Ik (Viento)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Ik.png';*/
                    break;
                case "Akbal":
                    /*var num = 13;
                    var text2 = 'Akbal (Noche)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Akbal.png';*/
                    break;
                case "Kan":
                    /*var num = 18;
                    var text2 = 'Kan (Semilla)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Kan.png';*/
                    break;
                case "Chicchan":
                    /*var num = 23;
                    var text2 = 'Chicchan (Serpiente)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Chikchan.png';*/
                    break;
                case "Cimi":
                    /*var num = 28;
                    var text2 = 'Cimi (Muerte)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Kimi.png';*/
                    break;
                case "Manik":
                    /*var num = 33;
                    var text2 = 'Manik (Venado)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Manik.png';*/
                    break;
                case "Lamat":
                    /*var num = 38;
                    var text2 = 'Lamat (Conejo)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Lamat.png';*/
                    break;
                case "Muluc":
                    /*var num = 43;
                    var text2 = 'Muluc (Agua)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Muluk.png';*/
                    break;
                case "Oc":
                    /*var num = 48;
                    var text2 = 'Oc (Perro)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Ok.png';*/
                    break;
                case "Chuen":
                    /*var num = 53;
                    var text2 = 'Chuen (Mono)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Chuen.png';*/
                    break;
                case "Eb":
                    /*var num = 58;
                    var text2 = 'Eb (Camino)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Eb.png';*/
                    break;
                case "Ben":
                    /*var num = 62;
                    var text2 = 'Ben (Junco)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Ben.png';*/
                    break;
                case "Ix":
                    /*var num = 68;
                    var text2 = 'Ix (Jaguar)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Ix.png';*/
                    break;
                case "Men":
                    /*var num = 72;
                    var text2 = 'Men (Águila)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Men.png';*/
                    break;
                case "Cib":
                    /*var num = 78;
                    var text2 = 'Cib (Lechuza)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Kid.png';*/
                    break;
                case "Caban":
                    /*var num = 82;
                    var text2 = 'Caban (Tierra)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Kaban.png';*/
                    break;
                case "Etznab":
                    /*var num = 88;
                    var text2 = 'Etznab (Roca)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Etznab.png';*/
                    break;
                case "Cauac":
                    /*var num = 93;
                    var text2 = 'Cauac (Tormenta)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Kawak.png';*/
                    break;
                case "Ahau":
                    /*var num = 98;
                    var text2 = 'Ahau (Luz)';
                    var img2 = 'http://b556df7d817b06260ee5-f1205ba1cfd77bb973cc0ffc1bcdf44e.r90.cf1.rackcdn.com/Jade/nahuales/nahuales/Ahau.png';*/
                    break;
                default :
                    /*var num = 3;
                    var text2 = '';
                    var img2 = '';*/
                    break;
            }
	}
}